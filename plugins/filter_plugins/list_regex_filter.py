import re
def list_regex_filter(list_,regex):
    pattern = re.compile(r''+regex)
    filtered = [i for i in list_ if not pattern.search(i)]
    return filtered


class FilterModule(object):
    def filters(self):
        return {
            'list_regex_filter': list_regex_filter,
        }
