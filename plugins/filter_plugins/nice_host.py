import socket
from ansible.errors import AnsibleError, AnsibleFilterError

def get_nice_host(value):
    try:
       return socket.gethostbyaddr(value)[0]
    except socket.gaierror as e:
        raise AnsibleFilterError('Error from parse hostname: {}\n{}'.format(value,e))
    except socket.herror as e:
        #raise AnsibleFilterError('Resolver error from parse hostname: {}\n{}'.format(value,e))
        return value

class FilterModule(object):
    def filters(self):
        return {
            'nice_host': get_nice_host,
        }
