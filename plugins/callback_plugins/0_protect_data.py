# -*- coding: utf-8 -*-
from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

from ansible.plugins.callback.default import CallbackModule as CallbackModule_default
import collections, locale, re
from pprint import pprint

import sys

reload(sys)
sys.setdefaultencoding('utf-8')

try:
    import simplejson as json
except ImportError:
    import json

encoding = locale.getpreferredencoding()


class CallbackModule(CallbackModule_default):
    CALLBACK_VERSION = 4.0
    CALLBACK_TYPE = 'stdout'
    CALLBACK_NAME = '0_protect_data'

    support_protect_data = True

    # Поля, по которым будет производится поиск переменных, потенциально содержащих пароли
    # Для оптимизации и увеличения скорости прогона деплоя убраны ключи: var
    FIELDS = ['cmd', 'command', 'msg', 'stdout', 'stderr', 'results', 'stdout_lines', 'stderr_lines',
            'value', 'ps', 'hostvars', 'ansible_facts',"invocation"]
    # , 'invocation' , 'item' ???

    # Наименовани ключей, значение которых необходимо исключить из поиска для ускорения времени выборки
    EXCLUDE_FIELD = ['common_roles_version_req','common_roles_path','common_search_artifact_group_dict', 'debug_version', 'artifact_content', 'artifacts_to_deploy',
                    'artifacts_to_remove', 'tmp_path', 'current_deploy_dir', 'artifact_path', 'xsd_file_path',
                    'xml_file_path', 'check_running', 'pid_file_must_exists', 'status_message', 'is_started',
                    'app_list',
                    'backend_failed', 'is_started', 'git_configure', 'app_deploy_tmp_path', 'app_deploy_artifact_path',
                    'find_path',
                    'common_current_version_current_version', 'filtered_artifacts', 'rollback_path', 'list_files',
                    'filtered_artifacts',
                    'tomcat_common_loader', 'path_version', 'current_version_nodes_list', 'directory', 'files',
                    'current_path', 'sysinfo', 'ansible_hostname', 'json_info_content', 'add_history_content',
                    'add_version_context', 'info_json', 'appinfo', 'ports_list', 'all_sr_content', 'git_key',
                    'artifact_version', 'head_groups_dict', 'file_path', 'artifact_name_lower', 'node_info',
                    'application_current_version', 'application_sr_node',
                    'application_name_sr', 'product_name_sr', 'app_history_sr', 'add_history_content',
                    'add_version_context', 'timestamp', 'deploy_result', 'copied_wars', 'check_hostname',
                    'path_to_ouptput_file',
                    'product_dir_path', 'memory_info', 'content_sysinfo_path', 'unreachable_host', 'stat']

    # Словари, содержащие в себе наборы элементов, в которых может находиться пароль
    HIDE_PASS_VARS = {}
    REGEX_PATTERNS = {}

    # Список паттернов, по которым находится переменная, в которой может содержаться пароль
    dictionary_passwords_key = [
        'password',
        'pwd',
        'syspass',
        'NodeValue',
        'decrypted_value',
        'api_key',
        '_pass'
    ]

    # Набор паттернов, предназначенный для обвязки результирующих секретов и поиске значений в тексте
    patterns = [
        (' ', '"'),
        ('"', '"'),
        ("'", "'"),
        ('\\"', '\\"'),
        (':', '@'),
        ('\\"', '@'),
        ('"', '@'),
        ("\\'", '@'),
        ("'", '@'),
        ("\\'", "\\'"),
        ('', '\\'),
        ('/', '@'),
        (' ', ' '),
    ]

    def __make_unhasheble(self, d):
        return json.loads(d)

    def __make_hashable(self, d):
        return json.dumps(d, sort_keys=True)

    def __multi_replace(self, string, replacements):
        """
            Функция предназначена для возможности замены содержимого в тексте по известным паттернам, за один проход
            :param str string: строка, в которой будут производиться изменения
            :param dict replacements: словарь содержащий в себе набор регулярных выражений для поиска и фраз для замены
            :rtype: str обработанный текст
        """
        res_rep = [replacement["pattern"] for key, replacement in replacements.items()]
        if len(res_rep)>0:
            pattern = re.compile("|".join(res_rep), 0)
            return pattern.sub(lambda match: replacements[match.group(0)]["replace"], string)
        else:
            return string

    def __generate_pattern_vars(self, v_var):
        """
        Генерирует словарь, для возможности замены элементов текста, за один прогон по регулярным выражениям
        :param k_var: Содержит наименование переменной (вплоть до родителя если используется словарь),
         в которой может содержаться секрет
        :param v_var: Содержит значение этой переменной
        :return:
            Словарь, со всем возможными вариантами появлений данной переменной в выводен и вариантами замены.

            Example:
                {' 1111': {'pattern': '\\ 1111$', 'replace': ' ***SECURE_MBUS_CM_PASSWORD***'},
                 ' 1111 ': {'pattern': '\\ 1111\\ ',
                            'replace': ' ***SECURE_MBUS_CM_PASSWORD*** '},
                 '"1111"': {'pattern': '\\"1111\\"',
                            'replace': '"***SECURE_MBUS_CM_PASSWORD***"'},
                 "'1111'": {'pattern': "\\'1111\\'",
                            'replace': "'***SECURE_MBUS_CM_PASSWORD***'"},
                 '/1111@': {'pattern': '\\/1111\\@',
                            'replace': '/***SECURE_MBUS_CM_PASSWORD***@'},
                 u'1111': {'pattern': '^1111$', 'replace': u'***SECURE_MBUS_CM_PASSWORD***'},
                 '1111\\': {'pattern': '1111\\\\',
                            'replace': '***SECURE_MBUS_CM_PASSWORD***\\'},
                 ':1111': {'pattern': '\\:1111$', 'replace': ':***SECURE_MBUS_CM_PASSWORD***'},
                 ':1111@': {'pattern': '\\:1111\\@',
                            'replace': ':***SECURE_MBUS_CM_PASSWORD***@'},
                 '\\"1111\\"': {'pattern': '\\\\\\"1111\\\\\\"',
                                'replace': '\\"***SECURE_MBUS_CM_PASSWORD***\\"'},
                 "\\'1111\\'": {'pattern': "\\\\\\'1111\\\\\\'",
                                'replace': "\\'***SECURE_MBUS_CM_PASSWORD***\\'"}}

        """
        replace_note = '***SECURE_PASSWORD_VARIABLE***'
        for pattern in self.patterns:
            rep_pattern = {"pattern": r'', "replace": ""}
            if pattern[0] == '^' and pattern[1] == '$':
                rep_pattern["pattern"] = '{0}{1}{2}'.format(pattern[0], re.escape(v_var), pattern[1])
                rep_pattern["replace"] = replace_note
                self.HIDE_PASS_VARS[v_var] = rep_pattern
            elif pattern[0] in [':', ' '] and pattern[1] == '$':
                rep_pattern["pattern"] = '{0}{1}{2}'.format(re.escape(pattern[0]), re.escape(v_var), pattern[1])
                rep_pattern["replace"] = '{0}{1}'.format(pattern[0], replace_note)
                self.HIDE_PASS_VARS['{0}{1}'.format(pattern[0], v_var)] = rep_pattern
            else:
                rep_pattern["pattern"] = re.escape('{0}{1}{2}'.format(pattern[0], v_var, pattern[1]))
                rep_pattern["replace"] = '{0}{1}{2}'.format(pattern[0], replace_note, pattern[1])
                self.HIDE_PASS_VARS['{0}{1}{2}'.format(pattern[0], v_var, pattern[1])] = rep_pattern

    def hide_from_dict(self, data):
        """
        Функция предназначена для замены элементов callback словаря элементами из self.HIDE_PASS_VARS @dict
        :param data: - @dict items from ansible callback data
        :return:  - moded  ansible callback data

        Example:
            If you running task:
            "
                - name: Set secret get_vars
                  set_fact:
                    test_password: 'pass_from_test'
            "
            Ansible will be generate callback data:
            "
                {'_ansible_no_log': False,
                 '_ansible_verbose_always': True,
                 'ansible_facts': {u'test_password': u'pass_from_test'},
                 'changed': False,
                 'failed': False}
            "

            After all recursive manipulation callback data will be modified and find values will be hide:
            "
                {'_ansible_no_log': False,
                 '_ansible_verbose_always': True,
                 'ansible_facts': {u'test_password': u'***SECURE_ANSIBLE_FACTS.TEST_PASSWORD***'},
                 'changed': False,
                 'failed': False}
            "

            And your see new result in terminal:
            "
                ok: [b2b_datamart_back] => {
                    "ansible_facts": {
                        "test_password": "***SECURE_ANSIBLE_FACTS.TEST_PASSWORD***"
                    },
                    "changed": false,
            "

        """
        ret = self.__make_hashable(data)
        #pprint(ret)
        ret = self.__multi_replace(ret, self.HIDE_PASS_VARS)
        #pprint(ret)
        ret = self.__make_unhasheble(ret)
        #pprint(ret)
        return ret

    def hide_secrets_from_fields(self, data):
        """
        Производит итерирование по словарю ansible callback data, в случае,
         если верхнеуровневые элементы есть в self.FIELDS, производит поиск в дочерних элементах.
        Модифицирует дочерние элементы только в тех случаях, если их ключи отсутствуют в наборе: self.EXCLUDE_FIELD
        :param data: @dict items from ansible callback data
        :return: Возвращает скорректированный набор данных функцией hide_from_dict
        """
        ret = {}
        #pprint(data)
        for k_d, v_d in data.items():
            if k_d in self.FIELDS:
                #pprint(v_d)
                ret[k_d] = self.hide_from_dict(v_d)
            else:
                ret[k_d] = v_d
        return ret

    def search_hide_password(self, data, key_field):
        """
        Производит поиск переменных, в которых могут содержаться секреты
        и накапливает словарь self.HIDE_PASS_VARS их именами и значениями
        :param data: @dict items from ansible callback data
        :param search_in_facts: ключ предназначенный для полного поиска секретов из result словаря
        :param prev_key: Содержит имя предыдущего элемента, по которому осуществялся поиск
        """

        if isinstance(data, collections.Mapping):
            for key, value in data.items():
                if isinstance(value, collections.Mapping):
                    self.search_hide_password(value, key)
                elif type(value) is list and len(value) > 0:
                    for item in value:
                        if isinstance(item, collections.Mapping):
                            self.search_hide_password(item, key)
                        else:
                            for find_keys in self.dictionary_passwords_key:
                                if find_keys.lower() in key.lower():
                                    self.__generate_pattern_vars(item)
                                    break
                else:
                    if value is not None and not isinstance(value, (int, long, float, complex, bool)):
                        for find_keys in self.dictionary_passwords_key:
                            if find_keys.lower() in key.lower():
                                if len(value) > 0:
                                    self.__generate_pattern_vars(value)
                                break
        elif type(data) is list:
            index = 1
            for item in data:
                if isinstance(item, collections.Mapping):
                    self.search_hide_password(item, key_field)
                else:
                    for find_keys in self.dictionary_passwords_key:
                        if find_keys.lower() in key_field.lower():
                            # pprint({"DEBUG1":{key:item}})
                            self.__generate_pattern_vars(item)
                            break
                index += 1
        else:
            if data is not None and not isinstance(data, (int, long, float, complex, bool)):
                for find_keys in self.dictionary_passwords_key:
                    if find_keys.lower() in key_field.lower():
                        if len(data) > 0:
                            # pprint({"DEBUG2":{key:value}})
                            self.__generate_pattern_vars(data)
                        break

    def search_hide_password_from_fields(self, data):
        """
        Производит поиск переменных, в которых могут содержаться секреты и только для ключей,
         которые имеются в ansible_facts.
        :param data: @dict items from ansible callback data
        :return:
        """
        for k_d, v_d in data.items():
            if isinstance(v_d, collections.Mapping):
                for buf_key, buf_value in v_d.items():
                    self.search_hide_password(buf_value, buf_key)
            elif type(v_d) is list:
                index = 1
                for item in v_d:
                    if isinstance(item, collections.Mapping):
                        for k_i, v_i in item.items():
                            self.search_hide_password(v_i, k_i)
                    index += 1
            else:
                if v_d is not None and not isinstance(v_d, (int, long, float, complex, bool)):
                    for find_keys in self.dictionary_passwords_key:
                        if find_keys.lower() in k_d.lower():
                            if len(v_d) > 0:
                                self.__generate_pattern_vars(v_d)
                            break

    #### Recursive find dict values
    def correct_callback(self, result):
        """
        Функция предназанченная для исправления dict ключей ansible callback
        :param result:
        :return:
        """
        ret = {}
        if isinstance(result, collections.Mapping):
            for key, value in result.iteritems():
                if isinstance(value, collections.Mapping):
                    ret[key] = self.correct_callback(value)
                else:
                    ret[key] = self._format_output(value)
        else:
            ret = self._format_output(ret)
        return ret

    ####Correctin output
    def _format_output(self, output):
        """
        Функция предназначена для корректировки вывода ansible callback в части передачи текста с экраннированными переводами строк
        :param output:
        :return:
        """
        # Strip ansible.vars.unsafe_proxy.AnsibleUnsafeText
        if re.search(r'ansible.vars.unsafe_proxy.AnsibleUnsafeText', str(type(output))):
            string = r'(\S+\:\s+\S+)\s+\\n'
            if re.search(string, output):
                output = re.sub(r'[^(\S)]\\n', r'\s\\n', re.sub(r"\'$", '', re.sub(r"^u'", '', str(output))))
                output = output.split("\n")

        return output

    # https://stackoverflow.com/questions/46320821/ansible-callback-plugin-how-to-get-play-attribute-values-with-variables-expande
    # https://dev.to/jrop/creating-an-alerting-callback-plugin-in-ansible---part-i-1h0n
    @staticmethod
    def object_dump(obj):
        """
            Функция для работы в режиме debug, необходима для возможности сдампить объект и его аттрибуты
        """
        for attr in dir(obj):
            if hasattr(obj, attr):
                print("obj.%s = %s" % (attr, getattr(obj, attr)))

    def v2_playbook_on_play_start(self, play):
        """
            Модификация метода v2_playbook_on_play_start, в части добавления функции по поиску паролей в hostvars
        """
        if len(self.HIDE_PASS_VARS) == 0:
            play_vars = play.vars
            #pprint(play_vars)
            if ('support_protect_data' in play_vars and play_vars['support_protect_data'] == False):
                self.support_protect_data = False
                print("Отключен режим сокрытия переменных")
            else:
                self.search_hide_password_from_fields(play.get_variable_manager().get_vars())
        return CallbackModule_default.v2_playbook_on_play_start(self, play)

    # _dump_results and _get_item
    def _dump_results(self, result, indent=None, sort_keys=True, keep_invocation=False):
        """
            Модификация метода _dump_results.
            Происходят следующие операции:
            - поиск паролей в выводе callback функции
            - форматирование результирующего словаря, который поступит в stdout, в части сокрытия паролей
            - форматирование вывода параметров, которые содержат переводы строк

            P.S. _ansible_verbose_always = True - включает вывод в режиму json.dumps с идентацией
        """
        # Enable JSON identation
        result['_ansible_verbose_always'] = True
        try:
            if (self.support_protect_data == True):
                if 'skipped' not in result or ('skipped' in result and not result['skipped']):
                    if not "skip_reason" in result or ( 'skip_reason' in result and result['skip_reason'] != "Conditional result was False"): 
                        if 'ansible_facts' in result:
                            self.search_hide_password_from_fields(result['ansible_facts'])
                        result = self.hide_secrets_from_fields(result)
            result = self.correct_callback(result)
        except Exception as e:
            print(str(e))
        return CallbackModule_default._dump_results(self, result, indent=indent, sort_keys=sort_keys,
                                                    keep_invocation=keep_invocation)

    #def _get_item(self, result):
    #    # Enable JSON identation
    #    result['_ansible_verbose_always'] = True
    #    if 'skipped' not in result or ('skipped' in result and not result['skipped']):
    #        if 'ansible_facts' in result:
    #            self.search_hide_password_from_fields(result['ansible_facts'], True)
    #        result = self.hide_secrets_from_fields(result)
    #        result = self._format_output(result)
    #    return CallbackModule_default._get_item(self, result)
