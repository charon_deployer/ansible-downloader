from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

DOCUMENTATION = '''
    callback: skippy_errors
    callback_type: stdout
    requirements:
      - set as main display callback
    short_description: Ansible screen output that ignores skipped status
    version_added: "2.0"

    description:
        - This callback does the same as the default except it does not output skipped host/task/item status
'''

from ansible.plugins.callback.default import CallbackModule as CallbackModule_default


class CallbackModule(CallbackModule_default):

    '''
    This is the default callback interface, which simply prints messages
    to stdout when new callback events are received.
    '''

    CALLBACK_VERSION = 2.0
    CALLBACK_TYPE = 'stdout'
    CALLBACK_NAME = 'skippy_errors'
    CALLBACK_NEEDS_WHITELIST = True

    def v2_runner_on_skipped(self, result):
        pass

    def v2_runner_item_on_skipped(self, result):
        pass
    
    def v2_runner_on_failed(self, result, ignore_errors=False):
        if ignore_errors != False:
            CallbackModule_default.v2_runner_on_failed(self, result, ignore_errors)
