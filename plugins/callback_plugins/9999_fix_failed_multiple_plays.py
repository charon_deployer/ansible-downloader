# -*- coding: utf-8 -*-
from __future__ import (absolute_import, division, print_function)

__metaclass__ = type
from pprint import pprint
from ansible.plugins.callback import CallbackBase
from ansible import constants as C
from ansible.utils.color import colorize, hostcolor

import sys

class CallbackModule(CallbackBase):
    CALLBACK_VERSION = 4.0
    CALLBACK_TYPE = 'aggregate'
    CALLBACK_NAME = '9999_fix_failed_multiple_plays'

    def __init__(self, *args, **kwargs):
        super(CallbackModule, self).__init__(*args, **kwargs)
        self.play = None


    ####### ON_STAT_ERRORS ###########
    ###### This callback return Exit_code =1 if find  unreachable_hosts or failures_hosts
    def v2_playbook_on_stats(self, stats):
        """
            Модификация метода v2_playbook_on_stats.
            Необходима для корректировки exit_code, в случае если один из плэев в плэйбуке был выполнен с ошибкой.
        """
        # self._display.banner("PLAY RECAP")

        hosts = sorted(stats.processed.keys())
        unreachable_hosts = []
        failures_hosts = []
        for h in hosts:
            t = stats.summarize(h)
            if (t['unreachable'] > 0):
                unreachable_hosts.append(h)
            elif (t['failures'] > 0):
                failures_hosts.append(h)

        if len(unreachable_hosts) > 0:
            self._display.display(("Find failures_hosts: %s " % ','.join(unreachable_hosts)), color=C.COLOR_ERROR,
                                  stderr=True)
            sys.exit(1)

        elif len(failures_hosts) > 0:
            self._display.display(("Find failures_hosts: %s " % ','.join(failures_hosts)), color=C.COLOR_ERROR,
                                  stderr=True)
            sys.exit(1)
